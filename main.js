const adviceId = document.querySelector("#advice_id");
const advicetext = document.querySelector("#advice_text");
const adviceBtn = document.querySelector("#advice_btn");

const newADvice = function () {
  fetch("https://api.adviceslip.com/advice")
    .then((response) => {
      return response.json();
    })

    .then((dataAdvice) => {
      console.log(dataAdvice);
      const adviceNUmber = dataAdvice.slip.id;
      const advice = dataAdvice.slip.advice;
      adviceId.innerHTML = adviceNUmber;
      advicetext.innerHTML = advice;
    })
    .catch((err) => {
      console.log(err);
    });
};

adviceBtn.addEventListener("click", newADvice);
